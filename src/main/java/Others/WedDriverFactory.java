package Others;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WedDriverFactory {
    public static WebDriver getDriver(String name) {
        WebDriver driver=null;
        if ("chrome".equalsIgnoreCase(name)) {
            System.setProperty("webdriver.chrome.driver", "G://qa_java//secondtesthometask//src//main//resources//chromedriver.exe");
            driver = new ChromeDriver();
        }
        if ("firefox".equalsIgnoreCase(name)) {
            System.setProperty("webdriver.gecko.driver", "G://qa_java//secondtesthometask//src//main//resources//geckodriver.exe");
            driver = new FirefoxDriver();
        }
        if ("edge".equalsIgnoreCase(name)) {
            System.setProperty("webdriver.edge.driver", "G://qa_java//secondtesthometask//src//main//resources//MicrosoftWebDriver.exe");
            driver = new EdgeDriver();
        }
        return driver;
    }
}
