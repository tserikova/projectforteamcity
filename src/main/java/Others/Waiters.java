package Others;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiters {

    public static final int DELAY = 6;

    public static void waitingForVisibility(WebDriver driver, By element, int delay){
        WebDriverWait wait = new WebDriverWait(driver,delay);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));

        }
    }

