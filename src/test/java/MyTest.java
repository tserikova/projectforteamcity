import Others.Constants;
import Pages.HomePage;
import Pages.SearchPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static Helpers.BaseHelper.openSite;
import static Others.Constants.SITE_URL;
import static Others.Waiters.DELAY;
import static Others.Waiters.waitingForVisibility;
import static Others.WedDriverFactory.getDriver;
import static org.testng.Assert.assertTrue;

public class MyTest {
    WebDriver driver;

    @BeforeMethod
    public void systemPref(){
        driver = getDriver("chrome");

    }

    @Test
    public void siteIsOpen(){
        openSite(driver,SITE_URL);
        assertTrue(driver.findElement(HomePage.LOGO).isDisplayed());
        System.out.println("Site is open");
        driver.close();
    }
    @Test
    public void searchTesting(){
        openSite(driver,SITE_URL);
        driver.findElement(HomePage.SEARCH).click();
        driver.findElement(HomePage.SEARCH).sendKeys(Constants.VALID_SEARCH);
        driver.findElement(HomePage.SEARCH).sendKeys(Keys.ENTER);
        assertTrue(driver.findElement(SearchPage.SEARCH_RESULTS).isDisplayed());
        driver.close();


    }
}
